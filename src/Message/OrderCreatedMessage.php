<?php

namespace App\Message;

use App\DTO\OrderItemDTO;

class OrderCreatedMessage
{
    /**
     * @var OrderItemDTO
     */
    private OrderItemDTO $orderItemDTO;

    /**
     * @param OrderItemDTO $orderItemDTO
     */
    public function __construct(OrderItemDTO $orderItemDTO)
    {
        $this->orderItemDTO = $orderItemDTO;
    }

    /**
     * @return OrderItemDTO
     */
    public function getOrderItemDTO(): OrderItemDTO
    {
        return $this->orderItemDTO;
    }
}
