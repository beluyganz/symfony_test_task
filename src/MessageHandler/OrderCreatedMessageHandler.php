<?php

namespace App\MessageHandler;

use App\Message\OrderCreatedMessage;
use App\Service\OrderItemService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class OrderCreatedMessageHandler implements MessageHandlerInterface
{

    /**
     * @param OrderItemService $orderItemService
     */
    public function __construct(private readonly OrderItemService $orderItemService)
    {

    }

    /**
     * @param OrderCreatedMessage $message
     * @return void
     */
    public function __invoke(OrderCreatedMessage $message): void
    {
        // TODO написати окремий DTO для message
        $orderItemDTO = $message->getOrderItemDTO();
        $this->orderItemService->createOrderItem($orderItemDTO);
    }
}
