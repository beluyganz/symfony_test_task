<?php

namespace App\Controller\Api\V1;

use App\Message\OrderCreatedMessage;
use App\Entity\OrderItem;
use App\Service\OrderItemService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/orders', name: 'orders.')]
class OrderItemController extends BaseController
{
    private const NOT_FOUND_MESSAGE = 'Order not found';

    /**
     * @param OrderItemService $orderItemService
     * @param ValidatorInterface $validator
     * @param MessageBusInterface $messageBus
     */
    public function __construct(
        private readonly OrderItemService     $orderItemService,
        private readonly ValidatorInterface  $validator,
        private readonly MessageBusInterface $messageBus
    )
    {

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    #[Route(name: 'get_orders', methods: ['GET'])]
    public function getOrders(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', self::DEFAULT_PAGE);
        $limit = $request->query->getInt('limit', self::DEFAULT_LIMIT);

        $orders = $this->orderItemService->getPaginatedOrderItems($page, $limit);
        $totalItems = $this->orderItemService->getTotalOrderItems();

        return $this->paginate($orders, $page, $limit, $totalItems);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    #[Route('/{id}', name: 'get_order', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function getOrder(int $id): JsonResponse
    {
        try {
            $orderItem = $this->orderItemService->getOrderItem($id);
            if ($orderItem === null) {
                throw new NotFoundHttpException('Order item not found');
            }
            return $this->success($this->formatResponse($orderItem));
        } catch (NotFoundHttpException) {
            return $this->error(self::NOT_FOUND_MESSAGE, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    #[Route(name: 'create_order', methods: ['POST'])]
    public function createOrder(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $orderItemDTO = $this->orderItemService->createOrderItemDTO($data);
        $errors = $this->validator->validate($orderItemDTO);

        if (count($errors) > 0) {
            return $this->handleValidationErrors($errors);
        }

        $this->messageBus->dispatch(new OrderCreatedMessage($orderItemDTO));

        return $this->success(['message' => 'Order successfully added to the queue']);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/{id}', name: 'update_order', requirements: ['id' => '\d+'], methods: ['PUT'])]
    public function updateOrder(int $id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $orderItemDTO = $this->orderItemService->createOrderItemDTO($data, $id);
        $errors = $this->validator->validate($orderItemDTO);

        if (count($errors) > 0) {
            return $this->handleValidationErrors($errors);
        }

        $orderItem = $this->orderItemService->getOrderItem($id);
        $updatedOrderItem = $this->orderItemService->updateOrderItem($orderItem, $orderItemDTO);

        return $this->success($this->formatResponse($updatedOrderItem));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    #[Route('/{id}', name: 'delete_order', requirements: ['id' => '\d+'], methods: ['DELETE'])]
    public function deleteOrder(int $id): JsonResponse
    {
        try {
            $orderItem = $this->orderItemService->getOrderItem($id);
            $this->orderItemService->deleteOrderItem($orderItem);

            return $this->success(['message' => 'Order successfully deleted'], Response::HTTP_NO_CONTENT);
        } catch (NotFoundHttpException) {
            return $this->error(self::NOT_FOUND_MESSAGE, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @param OrderItem $orderItem
     * @return array
     */
    protected function formatResponse(OrderItem $orderItem): array
    {
        return [
            'id'       => $orderItem->getId(),
            'price'    => $orderItem->getPrice(),
            'quantity' => $orderItem->getQuantity(),
            'status'   => $orderItem->getStatus(),
            'productId'=> $orderItem->getProduct()->getId(),
        ];
    }
}
