<?php

namespace App\Controller\Api\V1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

abstract class BaseController extends AbstractController
{
    public const DEFAULT_PAGE = 1;
    public const DEFAULT_LIMIT = 50;
    public const INVALID_JSON_MESSAGE = 'Invalid or empty JSON received.';

    /**
     * @param array $data
     * @param int $status
     * @param array $headers
     * @return JsonResponse
     */
    protected function success(array $data, int $status = Response::HTTP_OK, array $headers = []): JsonResponse
    {
        $responseData = [
            'status' => 'success',
            'data'   => $data
        ];

        return $this->json(data: $responseData, status: $status, headers: $headers, context: []);
    }

    /**
     * @param string $message
     * @param int $status
     * @param array $headers
     * @return JsonResponse
     */
    protected function error(string $message, int $status = Response::HTTP_BAD_REQUEST, array $headers = []): JsonResponse
    {
        $responseData = [
            'status'  => 'error',
            'message' => $message
        ];

        return $this->json(data: $responseData, status: $status, headers: $headers, context: []);
    }

    /**
     * @param ConstraintViolationListInterface $errors
     * @return JsonResponse|null
     */
    protected function handleValidationErrors(ConstraintViolationListInterface $errors): ?JsonResponse
    {
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $violation) {
                $errorMessages[] = $violation->getMessage();
            }

            return $this->error(implode(', ', $errorMessages));
        }

        return null;
    }
    /**
     * @param $data
     * @param $currentPage
     * @param $perPage
     * @param $totalItems
     * @return JsonResponse
     */
    protected function paginate($data, $currentPage, $perPage, $totalItems): JsonResponse
    {
        $totalPages = ceil($totalItems / $perPage);

        $nextPage = $currentPage < $totalPages ? $currentPage + 1 : null;
        $prevPage = $currentPage > 1 ? $currentPage - 1 : null;

        // TODO винести у DTO
        return $this->json(
            data: [
                'data'       => $data,
                'pagination' => [
                    'currentPage' => $currentPage,
                    'perPage'     => $perPage,
                    'totalPages'  => $totalPages,
                    'totalItems'  => $totalItems,
                    'links'       => [
                        'next'  => $nextPage ? "/api/products?page={$nextPage}&limit={$perPage}" : null,
                        'prev'  => $prevPage ? "/api/products?page={$prevPage}&limit={$perPage}" : null,
                        'first' => "/api/products?page=1&limit={$perPage}",
                        'last'  => "/api/products?page={$totalPages}&limit={$perPage}"
                    ]
                ]
            ]
        );
    }
}
