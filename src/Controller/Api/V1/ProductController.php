<?php

namespace App\Controller\Api\V1;

use App\DTO\ProductDTO;
use App\Entity\Product;
use App\Service\ProductService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/products', name: 'products.')]
class ProductController extends BaseController
{
    public const NOT_FOUND_MESSAGE = 'Product not found';

    /**
     * @param ProductService $productService
     * @param ValidatorInterface $validator
     */
    public function __construct(
        private readonly ProductService     $productService,
        private readonly ValidatorInterface $validator
    )
    {

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    #[Route(name: 'get_products', methods: ['GET'])]
    public function getProducts(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', self::DEFAULT_PAGE);
        $limit = $request->query->getInt('limit', self::DEFAULT_LIMIT);

        $totalItems = $this->productService->getTotalProducts();
        $products = $this->productService->getPaginatedAndAggregatedProducts($page, $limit, $totalItems);

        return $this->paginate($products, $page, $limit, $totalItems);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    #[Route('/{id}', name: 'get_product', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function getProduct(int $id): JsonResponse
    {
        try {
            $product = $this->productService->getProduct($id);
            return $this->success($this->formatResponse($product));
        } catch (NotFoundHttpException) {
            return $this->error(self::NOT_FOUND_MESSAGE, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    #[Route(name: 'create_product', methods: ['POST'])]
    public function createProduct(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $productDTO = new ProductDTO(
            name: $data['name'],
            description: $data['description'],
            price: (float)$data['price']
        );

        $errors = $this->validator->validate($productDTO);

        if (count($errors) > 0) {
            return $this->handleValidationErrors($errors);
        }

        $product = $this->productService->createProduct($productDTO);

        return $this->success($this->formatResponse($product), Response::HTTP_CREATED);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/{id}', name: 'update_product', methods: ['PUT'])]
    public function updateProduct(int $id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $productDTO = new ProductDTO(
            name: $data['name'],
            description: $data['description'],
            price: (float)$data['price'],
            id: $id
        );

        $errors = $this->validator->validate($productDTO);

        if (count($errors) > 0) {
            return $this->handleValidationErrors($errors);
        }

        $product = $this->productService->getProduct($id);
        $updatedProduct = $this->productService->updateProduct($product, $productDTO);

        return $this->success($this->formatResponse($updatedProduct));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    #[Route('/{id}', name: 'delete_product', methods: ['DELETE'])]
    public function deleteProduct(int $id): JsonResponse
    {
        try {
            $this->productService->deleteProduct($id);
            return $this->success(null, Response::HTTP_NO_CONTENT);
        } catch (NotFoundHttpException) {
            return $this->error(self::NOT_FOUND_MESSAGE, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @param Product $product
     * @return array
     */
    protected function formatResponse(Product $product): array
    {
        return [
            'id'          => $product->getId(),
            'name'        => $product->getName(),
            'description' => $product->getDescription(),
            'price'       => $product->getPrice(),
        ];
    }
}
