<?php

namespace App\Command;

use App\Entity\Product;
use App\Entity\OrderItem;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use App\Service\StateService;

#[AsCommand(
    name: "app:create-data",
    description: "Creates random data for testing",
)]
class CreateDataCommand extends Command
{
    //private EntityManagerInterface $entityManager;

    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly StateService $stateService)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->stateService->startCommand();
        $faker = Factory::create();

        for ($i = 0; $i < 200; $i++) {
            $product = new Product();
            $product->setName($faker->word());
            $product->setDescription($faker->text());
            $product->setPrice($faker->randomFloat(2, 1, 100));

            $this->entityManager->persist($product);

            for ($j = 0; $j < 100; $j++) {
                $orderItem = new OrderItem();
                $orderItem->setProduct($product);
                $orderItem->setPrice($faker->randomFloat(2, 1, 100));
                $orderItem->setQuantity($faker->numberBetween(1, 10));
                $orderItem->setStatus($faker->randomElement(['new', 'processing', 'completed']));

                $this->entityManager->persist($orderItem);
            }
        }

        $this->entityManager->flush();

        $output->writeln('Data created successfully!');
        $this->stateService->stopCommand();

        return Command::SUCCESS;
    }
}
