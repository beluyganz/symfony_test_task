<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class ProductDTO
{
    /**
     * @var int|null
     */
    #[Assert\Type('integer')]
    private ?int $id = null;

    /**
     * @var string
     */
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    private string $name;

    /**
     * @var string
     */
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    private string $description;

    /**
     * @var float
     */
    #[Assert\NotBlank]
    #[Assert\Type('float')]
    private float $price;

    /**
     * @param string $name
     * @param string $description
     * @param float $price
     * @param int|null $id
     */
    public function __construct(string $name, string $description, float $price, ?int $id = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}

