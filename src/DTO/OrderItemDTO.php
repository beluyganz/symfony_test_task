<?php
namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class OrderItemDTO
{
    /**
     * @var int|null
     */
    #[Assert\Type('integer')]
    private ?int $id = null;

    /**
     * @var float
     */
    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\NotEqualTo(value: 0)]
    #[Assert\Type('float')]
    private float $price;

    /**
     * @var int
     */
    #[Assert\NotNull]
    #[Assert\NotEqualTo(value: 0)]
    #[Assert\NotBlank]
    #[Assert\Type('integer')]
    private int $quantity;

    /**
     * @var string
     */
    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    private string $status;

    /**
     * @var int
     */
    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\NotEqualTo(value: 0)]
    #[Assert\Type('integer')]
    private int $productId;

    /**
     * @param float $price
     * @param int $quantity
     * @param string $status
     * @param int $productId
     * @param int|null $id
     */
    public function __construct(float $price, int $quantity, string $status, int $productId, ?int $id = null)
    {
        $this->id = $id;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->status = $status;
        $this->productId = $productId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId(int $productId): self
    {
        $this->productId = $productId;

        return $this;
    }
}
