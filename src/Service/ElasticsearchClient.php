<?php

namespace App\Service;

use Elasticsearch\ClientBuilder;

class ElasticsearchClient
{
    /**
     * @var \Elasticsearch\Client
     */
    private $client;

    public function __construct()
    {
        $this->client = ClientBuilder::create()
            ->setHosts(['elasticsearch:9200']) // Замініть це на ваші Elasticsearch хости
            ->build();
    }

    /**
     * @return \Elasticsearch\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param $query
     * @return array|callable
     */
    public function search($query)
    {
        $params = [
            'index' => 'order_items',
            'body' => $query,
        ];

        return $this->getClient()->search($params);
    }
}

