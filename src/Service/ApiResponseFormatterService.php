<?php

namespace App\Service;

class ApiResponseFormatterService
{
    /**
     * @param array $data
     * @param string $link
     * @return array[]
     */
    public function formatDataWithLinks(array $data, string $link): array
    {
        $data['links'] = ['self' => $link];

        return ['data' => $data];
    }
}
