<?php

namespace App\Service;

use App\DTO\OrderItemDTO;
use App\Entity\OrderItem;
use App\Repository\OrderItemRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderItemService
{
    /**
     * @var OrderItemRepository
     */
    private OrderItemRepository $orderItemRepository;
    /**
     * @var ProductRepository
     */
    private ProductRepository $productRepository;

    /**
     * @param OrderItemRepository $orderItemRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(OrderItemRepository $orderItemRepository, ProductRepository $productRepository)
    {
        $this->orderItemRepository = $orderItemRepository;
        $this->productRepository = $productRepository;
    }

    // TODO розділити updateDTO createDTO

    /**
     * @param OrderItemDTO $orderItemDTO
     * @return OrderItem
     */
    public function createOrderItem(OrderItemDTO $orderItemDTO): OrderItem
    {
        $orderItem = new OrderItem();
        $this->updateOrderItemFromDTO($orderItem, $orderItemDTO);
        $this->orderItemRepository->save($orderItem);

        return $orderItem;
    }

    /**
     * @param OrderItem $orderItem
     * @param OrderItemDTO $orderItemDTO
     * @return OrderItem
     */
    public function updateOrderItem(OrderItem $orderItem, OrderItemDTO $orderItemDTO): OrderItem
    {
        $this->updateOrderItemFromDTO($orderItem, $orderItemDTO);
        $this->orderItemRepository->save($orderItem);

        return $orderItem;
    }

    /**
     * @param OrderItem $orderItem
     * @return void
     */
    public function deleteOrderItem(OrderItem $orderItem): void
    {
        $this->orderItemRepository->remove($orderItem);
    }

    /**
     * @param int $id
     * @return OrderItem|null
     */
    public function getOrderItem(int $id): ?OrderItem
    {
        $orderItem = $this->orderItemRepository->find($id);
        if (!$orderItem) {
            throw new NotFoundHttpException('Order not found');
        }
        return $orderItem;
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPaginatedOrderItems(int $page, int $limit): array
    {
        return $this->orderItemRepository->findPaginated($page, $limit);
    }

    /**
     * @return int
     */
    public function getTotalOrderItems(): int
    {
        return $this->orderItemRepository->countAll();
    }

    /**
     * @param OrderItem $orderItem
     * @param OrderItemDTO $orderItemDTO
     * @return void
     */
    private function updateOrderItemFromDTO(OrderItem $orderItem, OrderItemDTO $orderItemDTO): void
    {
        $orderItem->setPrice($orderItemDTO->getPrice());
        $orderItem->setQuantity($orderItemDTO->getQuantity());
        $orderItem->setStatus($orderItemDTO->getStatus());

        $product = $this->productRepository->find($orderItemDTO->getProductId());
        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        $orderItem->setProduct($product);
    }

    public function createOrderItemDTO(array $data, int $id = 0): OrderItemDTO
    {
        return new OrderItemDTO(
            price: (float) ($data['price'] ?? null),
            quantity: (int) ($data['quantity'] ?? null),
            status: $data['status'] ?? null,
            productId: (int) ($data['productId'] ?? null),
            id: $id
        );
    }
}
