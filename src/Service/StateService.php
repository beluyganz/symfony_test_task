<?php

namespace App\Service;

class StateService
{
    private bool $isCommandRunning = false;

    public function startCommand(): void
    {
        $this->isCommandRunning = true;
    }

    public function stopCommand(): void
    {
        $this->isCommandRunning = false;
    }

    public function isCommandRunning(): bool
    {
        return $this->isCommandRunning;
    }
}
