<?php
namespace App\Service;
readonly class ElasticsearchService
{

    /**
     * @param ElasticsearchClient $client
     */
    public function __construct(private ElasticsearchClient $client)
    {

    }

    /**
     * @param int $totalItems
     * @return array
     */
    private function buildSearchParams(int $totalItems): array
    {
        return [
            'size' => 0,
            'query' => [
                'term' => [
                    'status' => [
                        'value' => 'completed',
                    ],
                ],
            ],
            'aggs' => [
                'product_nested' => [
                    'nested' => [
                        'path' => 'product',
                    ],
                    'aggs' => [
                        'product_aggs' => [
                            'terms' => [
                                'field' => 'product.id',
                                'size' => $totalItems
                            ],
                            'aggs' => [
                                'product_to_root' => [
                                    'reverse_nested' => new \stdClass(),
                                    'aggs' => [
                                        'total_quantity' => [
                                            'sum' => [
                                                'field' => 'quantity',
                                            ],
                                        ],
                                        'total_sales' => [
                                            'sum' => [
                                                'script' => [
                                                    'source' => "params['_source']['quantity'] * params['_source']['price']",
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param int $totalItems
     * @return array
     */
    public function getAggregatedData(int $totalItems): array
    {
        $params = $this->buildSearchParams($totalItems);
        $response = $this->client->search($params);
        return $this->processResponse($response);
    }

    /**
     * @param array $response
     * @return array
     */
    private function processResponse(array $response): array
    {
        $buckets = $response['aggregations']['product_nested']['product_aggs']['buckets'];

        $aggregatedData = [];
        foreach ($buckets as $bucket) {
            $aggregatedData[$bucket['key']] = [
                'quantity_sold' => $bucket['product_to_root']['total_quantity']['value'],
                'total_sales' => $bucket['product_to_root']['total_sales']['value'],
            ];
        }

        return $aggregatedData;
    }
}
