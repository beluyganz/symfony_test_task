<?php

namespace App\Service;

use App\DTO\ProductDTO;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

readonly class ProductService
{
    /**
     * @param ProductRepository $productRepository
     * @param ElasticsearchService $elasticsearchService
     */
    public function __construct(
        private ProductRepository    $productRepository,
        private ElasticsearchService $elasticsearchService
    )
    {

    }

    /**
     * @param ProductDTO $productDTO
     * @return Product
     */
    public function createProduct(ProductDTO $productDTO): Product
    {
        $product = new Product();
        $this->updateProductFromDTO($product, $productDTO);

        $this->productRepository->save($product);

        return $product;
    }

    /**
     * @param Product $product
     * @param ProductDTO $productDTO
     * @return Product
     */
    public function updateProduct(Product $product, ProductDTO $productDTO): Product
    {
        $this->updateProductFromDTO($product, $productDTO);

        $this->productRepository->save($product);

        return $product;
    }

    /**
     * @param Product $product
     * @return void
     */
    public function deleteProduct(int $id): void
    {
        $product = $this->productRepository->find($id);
        $this->productRepository->remove($product);
    }

    /**
     * @param int $id
     * @return Product
     */
    public function getProduct(int $id): Product
    {
        $product = $this->productRepository->find($id);
        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        return $product;
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPaginatedProducts(int $page, int $limit): array
    {
        return $this->productRepository->findPaginated($page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @param int $totalItems
     * @return array
     */
    public function getPaginatedAndAggregatedProducts(int $page, int $limit, int $totalItems): array
    {
        $products = $this->getPaginatedProducts($page, $limit);
        $aggregatedData = $this->elasticsearchService->getAggregatedData($totalItems);

        return array_map(function($product) use ($aggregatedData) {
            $productId = $product->getId();

            return [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'quantity_sold' => $aggregatedData[$productId]['quantity_sold'] ?? 0,
                'total_sales' => $aggregatedData[$productId]['total_sales'] ?? 0,
            ];
        }, $products);
    }

    /**
     * @return int
     */
    public function getTotalProducts(): int
    {
        return $this->productRepository->countAll();
    }

    /**
     * @param Product $product
     * @param ProductDTO $productDTO
     * @return void
     */
    private function updateProductFromDTO(Product $product, ProductDTO $productDTO): void
    {
        $product->setName($productDTO->getName());
        $product->setDescription($productDTO->getDescription());
        $product->setPrice($productDTO->getPrice());
    }

    /**
     * @param array $data
     * @param int $id
     * @return ProductDTO
     */
    public function createProductDTO(array $data, int $id = 0): ProductDTO
    {
        return new ProductDTO(
            name: $data['name'] ?? null,
            description: $data['description'],
            price: (float) ($data['price'] ?? null),
            id: $id
        );
    }
}
