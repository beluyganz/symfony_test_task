<?php
namespace App\Repository;

use App\Entity\Product;
use App\Service\ElasticsearchClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     * @param ElasticsearchClient $elasticsearchClient
     */
    public function __construct(ManagerRegistry $registry, private readonly ElasticsearchClient $elasticsearchClient)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param Product $product
     * @return void
     */
    public function save(Product $product): void
    {
        $this->_em->persist($product);
        $this->_em->flush();
    }

    /**
     * @param Product $product
     * @return void
     */
    public function remove(Product $product): void
    {
        $this->_em->remove($product);
        $this->_em->flush();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findPaginated(int $page, int $limit): array
    {
        // TODO при можливості використати Query Builder
        $dql = "SELECT p FROM App\Entity\Product p";
        $query = $this->getEntityManager()->createQuery($dql)
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit);

        return $query->getResult();
    }

    /**
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countAll(): int
    {
        // TODO при можливості використати Query Builder
        return $this->getEntityManager()
            ->createQuery('SELECT COUNT(p.id) FROM App\Entity\Product p')
            ->getSingleScalarResult();
    }
}
