<?php
namespace App\Repository;

use App\Entity\OrderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

#[\Doctrine\ORM\Mapping\Entity(repositoryClass: OrderItemRepository::class)]
class OrderItemRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderItem::class);
    }

    /**
     * @param OrderItem $orderItem
     * @return void
     */
    public function save(OrderItem $orderItem): void
    {
        $this->_em->persist($orderItem);
        $this->_em->flush();
    }

    /**
     * @param OrderItem $orderItem
     * @return void
     */
    public function remove(OrderItem $orderItem): void
    {
        $this->_em->remove($orderItem);
        $this->_em->flush();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findPaginated(int $page, int $limit): array
    {
        // TODO при можливості використати Query Builder
        $dql = "SELECT o FROM App\Entity\OrderItem o";
        $query = $this->getEntityManager()->createQuery($dql)
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit);

        return $query->getResult();
    }

    /**
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countAll(): int
    {
        // TODO при можливості використати Query Builder
        return $this->getEntityManager()
            ->createQuery('SELECT COUNT(o.id) FROM App\Entity\OrderItem o')
            ->getSingleScalarResult();
    }
}
