<?php
namespace App\EventListener;

use App\Entity\OrderItem;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use FOS\ElasticaBundle\Elastica\Client;
use Elastica\Document;
use App\Service\StateService;

class OrderItemListener
{
    /**
     * @var Client
     */
    private Client $client;

    /**
     * OrderItemListener constructor.
     * @param Client $client
     * @param StateService $stateService
     * @description Initializes a new instance of the OrderItemListener class with the given Elastica client.
     */
    public function __construct(Client $client, private readonly StateService $stateService)
    {
        $this->client = $client;
    }

    /**
     * @param LifecycleEventArgs $args
     * @return void
     * @description Performs operations before an OrderItem entity is persisted. If the entity isn't an instance of OrderItem,
     *              the method will simply return. Otherwise, it will index the OrderItem entity using Elasticsearch.
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        if ($this->stateService->isCommandRunning()) {
            return;
        }

        $entity = $args->getObject();

        if (!$entity instanceof OrderItem) {
            return;
        }

        $this->indexOrderItem($entity);
    }

    /**
     * @param LifecycleEventArgs $args
     * @return void
     * @description Performs operations before an OrderItem entity is updated. If the entity isn't an instance of OrderItem,
     *              the method will simply return. Otherwise, it will index the updated OrderItem entity using Elasticsearch.
     */
    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof OrderItem) {
            return;
        }

        $this->indexOrderItem($entity);
    }

    /**
     * @param LifecycleEventArgs $args
     * @return void
     * @description Performs operations before an OrderItem entity is removed. If the entity isn't an instance of OrderItem,
     *              the method will simply return. Otherwise, it will remove the indexed OrderItem entity from Elasticsearch.
     */
    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof OrderItem) {
            return;
        }

        $index = $this->client->getIndex('order_items');
        $index->deleteById($entity->getId());
    }

    /**
     * @param OrderItem $orderItem
     * @return void
     * @description Indexes the given OrderItem entity into Elasticsearch.
     */
    private function indexOrderItem(OrderItem $orderItem): void
    {
        $index = $this->client->getIndex('order_items');

        $docData = [
            'id' => $orderItem->getId(),
            'price' => $orderItem->getPrice(),
            'quantity' => $orderItem->getQuantity(),
            'status' => $orderItem->getStatus(),
            'product' => [
                'id' => $orderItem->getProduct()->getId(),
                'name' => $orderItem->getProduct()->getName(),
                'description' => $orderItem->getProduct()->getDescription(),
                'price' => $orderItem->getProduct()->getPrice(),
            ],
        ];

        $document = new Document($orderItem->getId(), $docData);

        $index->addDocument($document);
        $index->refresh();
    }
}
