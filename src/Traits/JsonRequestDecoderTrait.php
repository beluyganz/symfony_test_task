<?php

namespace App\Traits;

use InvalidArgumentException;

trait JsonRequestDecoderTrait
{
    public function decodeJsonRequestContent(string $content): array
    {
        $data = json_decode($content, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new InvalidArgumentException('Invalid JSON.');
        }

        if (empty($data)) {
            throw new InvalidArgumentException('Empty JSON.');
        }

        return $data;
    }
}
