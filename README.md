# Getting Started

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/) (v2.10+)
2. Run `docker compose build --pull --no-cache` to build fresh images
3. Run `docker compose up -d` (the logs will be displayed in the current shell)
4. Open `https://localhost` in your favorite web browser and [accept the auto-generated TLS certificate](https://stackoverflow.com/a/15076602/1352334)

Now, you're ready to set up your database and prepare it for data:

5. Run `php bin/console app:create-data` to generate random testing data for your application. This command creates 200 products with random names, descriptions, and prices. For each product, it also creates 100 order items with random prices, quantities, and statuses. After running this command, check your database to see the generated data.

Next, reset and populate your ElasticSearch index:

6. Run `php bin/console fos:elastica:reset` to reset your ElasticSearch indexes and types.
7. Run `php bin/console fos:elastica:populate` to populate your ElasticSearch indexes with data.

Finally, start consuming messages from your Messenger component's transport:

8. Run `php bin/console messenger:consume async` to start a worker that will consume messages from your 'async' transport.

To stop the Docker containers, use the following command:

9. Run `docker compose down --remove-orphans`
